export default {
	ALLOW_SETUP: false,
	DATABASE_PATH: '/db/app.sqlite.db',
	ENCRYPTION_KEY: null,
}
