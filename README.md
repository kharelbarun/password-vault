# Password Vault

Password Vault allows you to store password locally in encrypted form.

## Setup
0. `git clone` this repository
1. Copy `env.js` into `env.local.js`
1. Update `ALLOW_SETUP` in `env.local.js` to allow setup:
	```
	ALLOW_SETUP: true
	```
1. Generate encryption-key using `generate-key` command:
	```
	./vault.js generate-key
	```
1. Copy the generated encryption-key into `env.local.js`:
	```
	ENCRYPTION_KEY: 'g.e.n.e.r.a.t.e.d...k.e.y',
	```
1. Setup database
	```
	./vault.js db:setup
	```

## Usage

Run `vault.js` to show available commands

1. List applications of which password is stored
	```
	./vault.js list
	```
1. Add new password for an application
	```
	./vault.js add
	```
1. Copy password to clipboard
	```
	./vault.js copy
	```
1. Show password
	```
	./vault.js show
	```
1. Update password
	```
	./vault.js update
	```
1. Remove password
	```
	./vault.js remove
	```
