#!/usr/bin/env node

import path from 'path';
import { fileURLToPath } from 'url';
import bootstrap from './js/bootstrap.js';
import env from './env.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let localEnv = {};
try {
	localEnv = (await import('./env.local.js')).default;
} catch {}

bootstrap(__dirname, {...env, ...localEnv});
