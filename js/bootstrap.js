import { getAppConfig } from "./config.js";
import initCommands from "./modules/initCommands.js";
export default function bootstrap(appPath, env) {
    const appConfig = getAppConfig(appPath, env);
    initCommands(appConfig);
}
