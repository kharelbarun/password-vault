import { promisifiedQuestion } from "./promisifiedQuestion.js";
export async function prompt(interface_, query, emptyQuery) {
    let userInput;
    do {
        userInput = await promisifiedQuestion(interface_, query);
        userInput = userInput.trim();
        if (userInput) {
            break;
        }
        console.log(emptyQuery);
    } while (true);
    return userInput;
}
