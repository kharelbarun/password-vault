export async function promisifiedQuestion(rl, query) {
    return new Promise((resolve) => rl.question(query, resolve));
}
