import crypto from "crypto";
import { ALGORITHM, ENCRYPTED_PASSWORD_ENCODING, ENCRYPTION_KEY_ENCODING, UNENCRYPTED_PASSWORD_ENCODING } from "./constants.js";
export function encrypt(key, nonEncrypted) {
    const cipher = crypto.createCipheriv(ALGORITHM, Buffer.from(key, ENCRYPTION_KEY_ENCODING), null);
    let encrypted = cipher.update(nonEncrypted, UNENCRYPTED_PASSWORD_ENCODING, ENCRYPTED_PASSWORD_ENCODING);
    encrypted += cipher.final(ENCRYPTED_PASSWORD_ENCODING);
    return encrypted;
}
export function decrypt(key, encrypted) {
    const cipher = crypto.createDecipheriv(ALGORITHM, Buffer.from(key, ENCRYPTION_KEY_ENCODING), null);
    let decrypted = cipher.update(encrypted, ENCRYPTED_PASSWORD_ENCODING, UNENCRYPTED_PASSWORD_ENCODING);
    decrypted += cipher.final(UNENCRYPTED_PASSWORD_ENCODING);
    return decrypted;
}
