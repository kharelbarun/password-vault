export function getAppConfig(appPath, env) {
    return {
        allowSetup: env.ALLOW_SETUP,
        appPath,
        databasePath: env.DATABASE_PATH,
        encryptionKey: env.ENCRYPTION_KEY,
    };
}
