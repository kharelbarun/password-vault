import initDatabase from "../modules/initDatabase.js";
import Vault from "../model/Vault.js";
import { encrypt } from "../modules/encryption.js";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";
export function updateCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const interface_ = createInterface();
        try {
            await update(interface_);
        }
        finally {
            interface_.close();
        }
    }
    async function update(interface_) {
        let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');
        const vault = await Vault.findOne({ where: { id: id } });
        if (vault === null) {
            console.log('Not found!');
            return;
        }
        console.log(`You are about to change password of service "${vault.serviceName}" and identifier "${vault.identifier}".`);
        let password = await prompt(interface_, 'Enter new password: ', 'Password cannot be empty.');
        const [affectedCount] = await Vault.update({
            encryptedPassword: encrypt(appConfig.encryptionKey, password),
        }, {
            where: { id }
        });
        if (affectedCount) {
            console.log('Update was successful!');
        }
        else {
            console.log('Update failed!');
        }
    }
    const updateCommand = {
        command: 'update',
        describe: 'Update password for an application',
        handler,
    };
    return updateCommand;
}
