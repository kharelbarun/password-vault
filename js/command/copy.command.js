import { exec } from "child_process";
import { promisify } from "util";
import Vault from "../model/Vault.js";
import { decrypt } from "../modules/encryption.js";
import initDatabase from "../modules/initDatabase.js";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";
const promisifiedExec = promisify(exec);
export function copyCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const interface_ = createInterface();
        try {
            await copy(interface_);
        }
        finally {
            interface_.close();
        }
    }
    async function copy(interface_) {
        let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');
        const vault = await Vault.findOne({ where: { id: id } });
        if (vault === null) {
            console.log('Not found!');
            return;
        }
        const decryptedPassword = decrypt(appConfig.encryptionKey, vault.encryptedPassword);
        try {
            await promisifiedExec('which xsel');
        }
        catch (_a) {
            console.error('"xsel" is not installed.');
            return;
        }
        try {
            await promisifiedExec(`echo -n ${decryptedPassword} | xsel -ib`);
        }
        catch (_b) {
            console.error('Error occurred when copying the password to clipboard.');
            return;
        }
        console.log('Password was successfully copied to the clipboard.');
    }
    const copyCommand = {
        command: 'copy',
        describe: 'Copy password of an application into clipboard',
        handler,
    };
    return copyCommand;
}
