import Vault from "../model/Vault.js";
import { encrypt } from "../modules/encryption.js";
import initDatabase from "../modules/initDatabase.js";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";
export function addCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const interface_ = createInterface();
        try {
            await add(interface_);
        }
        finally {
            interface_.close();
        }
    }
    async function add(interface_) {
        let serviceName = await prompt(interface_, 'Enter service name: ', 'Service name cannot be empty.');
        let identifier = await prompt(interface_, 'Enter identifier (username/email): ', 'Identifier cannot be empty.');
        let password = await prompt(interface_, 'Enter password: ', 'Password cannot be empty.');
        await Vault.create({
            serviceName,
            identifier,
            encryptedPassword: encrypt(appConfig.encryptionKey, password),
        });
        console.log('Password added successfully!');
    }
    const addCommand = {
        command: 'add',
        describe: 'Add password for an application',
        handler,
    };
    return addCommand;
}
