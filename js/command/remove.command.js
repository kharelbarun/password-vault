import Vault from "../model/Vault.js";
import initDatabase from "../modules/initDatabase.js";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";
export function removeCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const interface_ = createInterface();
        try {
            show(interface_);
        }
        catch (_a) {
            interface_.close();
        }
    }
    async function show(interface_) {
        let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');
        const vault = await Vault.findOne({ where: { id: id } });
        if (vault === null) {
            console.log('Not found!');
            return;
        }
        vault.destroy();
        console.log(`Password deleted for service ${vault.serviceName} and identifier ${vault.identifier}`);
    }
    const removeCommand = {
        command: 'remove',
        describe: 'Remove password for an application',
        handler,
    };
    return removeCommand;
}
