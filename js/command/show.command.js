import Vault from "../model/Vault.js";
import { decrypt } from "../modules/encryption.js";
import initDatabase from "../modules/initDatabase.js";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";
export function showCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const interface_ = createInterface();
        try {
            show(interface_);
        }
        catch (_a) {
            interface_.close();
        }
    }
    async function show(interface_) {
        let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');
        const vault = await Vault.findOne({ where: { id: id } });
        if (vault === null) {
            console.log('Not found!');
        }
        else {
            const decryptedPassword = decrypt(appConfig.encryptionKey, vault.encryptedPassword);
            console.log(`Decrypted password: ${decryptedPassword}`);
        }
    }
    const showCommand = {
        command: 'show',
        describe: 'Show password of an application',
        handler,
    };
    return showCommand;
}
