import initDatabase from "../modules/initDatabase.js";
export function dbSetupCommandFactory(appConfig) {
    async function handler() {
        await initDatabase(appConfig);
        const sequelize = appConfig.sequelize;
        await sequelize.sync();
    }
    const setupCommand = {
        command: 'db:setup',
        describe: 'Setup database tables',
        handler,
    };
    return setupCommand;
}
