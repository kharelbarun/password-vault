import { CreationOptional, InferAttributes, InferCreationAttributes, Model } from "sequelize";

export default
class Vault extends Model<InferAttributes<Vault>, InferCreationAttributes<Vault>>
{
	declare id: CreationOptional<number>;
	declare serviceName: string;
	declare identifier: string;
	declare encryptedPassword: string;
}
