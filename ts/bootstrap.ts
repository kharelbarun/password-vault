import { AppEnv, getAppConfig } from "./config.js";
import initCommands from "./modules/initCommands.js";

export default
function bootstrap(appPath: string, env: AppEnv)
{
	const appConfig = getAppConfig(appPath, env);
	initCommands(appConfig);
}
