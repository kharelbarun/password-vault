import yargs, { CommandModule } from "yargs";
import { AppConfig } from "../config.js";
import { showCommandFactory } from "../command/show.command.js";
import { copyCommandFactory } from "../command/copy.command.js";
import { addCommandFactory } from "../command/add.command.js";
import { removeCommandFactory } from "../command/remove.command.js";
import generateKeyCommand from "../command/generateKey.command.js";
import { listCommandFactory } from "../command/list.command.js";
import { dbSetupCommandFactory } from "../command/db.setup.command.js";
import { updateCommandFactory } from "../command/update.command.js";

export default
function initCommands(appConfig: AppConfig)
{
	const argv = yargs(process.argv.slice(2));

	const commands: CommandModule<{},any>[] = [
		copyCommandFactory(appConfig),
		listCommandFactory(appConfig),
		showCommandFactory(appConfig),
		addCommandFactory(appConfig),
		updateCommandFactory(appConfig),
		removeCommandFactory(appConfig),
		generateKeyCommand,
	];

	if (appConfig.allowSetup) {
		commands.push(dbSetupCommandFactory(appConfig));
	}

	commands.forEach(command => {
		argv.command(command);
	});

	argv.demandCommand()
	.help()
	.argv;
}
