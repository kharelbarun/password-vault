import readline from "readline";
import { promisifiedQuestion } from "./promisifiedQuestion.js";

export
async function prompt(interface_: readline.Interface, query: string, emptyQuery: string)
{
	let userInput;
	do {
		userInput = await promisifiedQuestion(interface_, query);
		userInput = userInput.trim();
		if (userInput) {
			break;
		}
		console.log(emptyQuery);
	} while (true);

	return userInput;
}
