import { Sequelize } from "sequelize";
import { AppConfig } from "../config.js";
import initModels from "./initModels.js";

export default
async function initDatabase(appConfig: AppConfig): Promise<Sequelize>
{
	if (appConfig.sequelize) {
		return appConfig.sequelize;
	}

	const sequelize = new Sequelize({
		logging: false,
		dialect: 'sqlite',
		storage:  appConfig.appPath + appConfig.databasePath,
	});

	initModels(sequelize);

	await sequelize.sync();

	appConfig.sequelize = sequelize;

	return sequelize;
}
