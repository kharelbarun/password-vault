import readline from "readline";

export
async function promisifiedQuestion(rl: readline.Interface, query: string)
{
	return new Promise<string>((resolve) => rl.question(query, resolve));
}
