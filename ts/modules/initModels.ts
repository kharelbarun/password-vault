import { DataTypes, Sequelize } from "sequelize";
import Vault from "../model/Vault.js";

export default
function initModels(sequelize: Sequelize)
{
	Vault.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		serviceName: {
			type: DataTypes.STRING,
		},
		identifier: {
			type: DataTypes.STRING,
		},
		encryptedPassword: {
			type: DataTypes.TEXT,
		},
	}, {
		sequelize
	});
}
