export
const ALGORITHM = 'aes-256-ecb';

export
const ENCRYPTION_KEY_ENCODING = 'hex';

export
const UNENCRYPTED_PASSWORD_ENCODING = 'utf-8';

export
const ENCRYPTED_PASSWORD_ENCODING = 'hex';

export
const ENCRYPTION_KEY_BYTE_LENGTH = 32;
