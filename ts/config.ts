import { Sequelize } from "sequelize";

export
type AppConfig = {
	allowSetup: boolean,
	appPath: string,
	databasePath: string,
	encryptionKey: string,
	sequelize?: Sequelize,
};

export
type AppEnv = {
	ALLOW_SETUP: boolean,
	DATABASE_PATH: string,
	ENCRYPTION_KEY: string,
};

export
function getAppConfig(appPath: string, env: AppEnv): AppConfig
{
	return {
		allowSetup: env.ALLOW_SETUP,
		appPath,
		databasePath: env.DATABASE_PATH,
		encryptionKey: env.ENCRYPTION_KEY,
	};
}
