import { CommandModule } from "yargs";
import initDatabase from "../modules/initDatabase.js";
import { AppConfig } from "../config.js";

export
function dbSetupCommandFactory(appConfig: AppConfig)
{
	async function handler()
	{
		await initDatabase(appConfig);
		const sequelize = appConfig.sequelize!;

		await sequelize.sync();
	}

	const setupCommand: CommandModule<{},{}> = {
		command: 'db:setup',
		describe: 'Setup database tables',
		handler,
	};

	return setupCommand;
}
