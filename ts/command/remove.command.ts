import { CommandModule } from "yargs";
import Vault from "../model/Vault.js";
import { AppConfig } from "../config.js";
import initDatabase from "../modules/initDatabase.js";
import { Interface } from "readline";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";

export
function removeCommandFactory(appConfig: AppConfig)
{
	async function handler()
	{
		await initDatabase(appConfig);

		const interface_ = createInterface();
		try {
			show(interface_);
		} catch {
			interface_.close();
		}
	}

	async function show(interface_: Interface)
	{
		let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');

		const vault = await Vault.findOne({ where: { id: id }});

		if (vault === null) {
			console.log('Not found!');
			return;
		}

		vault.destroy();

		console.log(`Password deleted for service ${vault.serviceName} and identifier ${vault.identifier}`);
	}

	const removeCommand: CommandModule<{},{}> = {
		command: 'remove',
		describe: 'Remove password for an application',
		handler,
	};

	return removeCommand;
}
