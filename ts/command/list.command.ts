import { CommandModule } from "yargs";
import initDatabase from "../modules/initDatabase.js";
import { AppConfig } from "../config.js";
import Vault from "../model/Vault.js";

export
function listCommandFactory(appConfig: AppConfig)
{
	async function handler()
	{
		await initDatabase(appConfig);
		await list();
	}

	async function list()
	{
		const vaults = await Vault.findAll({ order: ['id'] });

		if (vaults.length === 0) {
			console.log('No apps registered.');
			return;
		}

		const pickAttributes = (vault: Vault) => (
			({ id, serviceName, identifier }) => ({ id, serviceName, identifier })
		)(vault);

		console.table(vaults.map((vault) => pickAttributes(vault)));
	}

	const listCommand: CommandModule<{},{}> = {
		command: 'list',
		describe: 'List all the services for which password are stored',
		handler,
	};

	return listCommand;
}
