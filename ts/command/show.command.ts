import { CommandModule } from "yargs";
import Vault from "../model/Vault.js";
import { decrypt } from "../modules/encryption.js";
import { AppConfig } from "../config.js";
import initDatabase from "../modules/initDatabase.js";
import { Interface } from "readline";
import { createInterface } from "../modules/createInterface.js";
import { prompt } from "../modules/prompt.js";

export
function showCommandFactory(appConfig: AppConfig)
{
	async function handler()
	{
		await initDatabase(appConfig);

		const interface_ = createInterface();
		try {
			show(interface_);
		} catch {
			interface_.close();
		}
	}

	async function show(interface_: Interface)
	{
		let id = await prompt(interface_, 'Enter service ID: ', 'ID cannot be empty.');

		const vault = await Vault.findOne({ where: { id: id }});

		if (vault === null) {
			console.log('Not found!');
		} else {
			const decryptedPassword = decrypt(appConfig.encryptionKey, vault.encryptedPassword);
			console.log(`Decrypted password: ${decryptedPassword}`);
		}
	}

	const showCommand: CommandModule<{},{}> = {
		command: 'show',
		describe: 'Show password of an application',
		handler,
	};

	return showCommand;
}
