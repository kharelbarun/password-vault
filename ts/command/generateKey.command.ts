import { CommandModule } from "yargs";
import crypto from "crypto";
import { ENCRYPTION_KEY_BYTE_LENGTH, ENCRYPTION_KEY_ENCODING } from "../modules/constants.js";

function handler()
{
	console.log('Key has been generated and is displayed below. Copy and paste it into env file.');
	console.log(crypto.randomBytes(ENCRYPTION_KEY_BYTE_LENGTH).toString(ENCRYPTION_KEY_ENCODING));
}

const generateKeyCommand: CommandModule<{},{}> = {
	command: 'generate-key',
	describe: 'Generate encryption-key to be stored in env file.',
	handler,
};

export default generateKeyCommand;
